﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Assets._Scripts
{
    class Session
    {
        private readonly static String FILEPATH = @"c:\temp\";

        private static Session instance;

        private readonly DateTime startTime = DateTime.Now;
        private DateTime softStartTime;
        private bool softStarted = false;
        private readonly int chance = new Random().Next(0, 4);

        private Session() { }

        public static Session Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Session();
                }
                return instance;
            }
        }

        public static void Init()
        {
            if (!Directory.Exists(FILEPATH))
            {
                Directory.CreateDirectory(FILEPATH);
            }

            if (!File.Exists(filename()))
            {
                using (StreamWriter sw = File.CreateText(filename()))
                {
                    sw.WriteLine("Horario,Pontuacao Acumulada,Pontuacao,Evento,Duracao");
                }
                insertLine("", "", "INICIO");
            }
        }

        public static void SoftStart()
        {
            if(!Session.Instance.softStarted)
            {
                Session.Instance.softStarted = true;
                Session.Instance.softStartTime = DateTime.Now;
            }
            Event("ESPACO_INICIO");
        }

        public static void Append(String score, String scoreDelta)
        {
            insertLine(score, scoreDelta, "PONTOS");
        }

        public static void Event(String eventName)
        {
            insertLine("", "", eventName);
        }

        public static int Chance()
        {
            return Session.Instance.chance;
        }


        private static void insertLine(String score, String scoreDelta, String eventName)
        {
            using (StreamWriter sw = File.AppendText(filename()))
            {
                sw.WriteLine(currentTime() + "," + score + "," +scoreDelta + "," + eventName + "," + elapsedTime());
            }
        }

        private static String elapsedTime()
        {
            if (!Session.Instance.softStarted)
            {
                return "";
            }
            else
            {
                DateTime elapsedTime = new DateTime((DateTime.Now - Session.Instance.softStartTime).Ticks);
                return elapsedTime.ToString("mm:ss.fff");
            }
        }
        private static String filename()
        {
            return FILEPATH + "log_sessao_" + Session.Instance.startTime.ToString("dd-MM-yyyy_HH-mm-ss") + ".csv";
        }

        private static String currentTime()
        {
            return DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss.fff");
        }

    }
}
