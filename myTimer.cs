﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Assets._Scripts;

public class myTimer : MonoBehaviour {

	public Text Timer;
	public Text Pfinal;
	public float myCoolTimer = 601;
	public bool freeTimer = false;
	public GameObject EstrelasLado;
	public GameObject EstrelasFrente;
	public GameObject EstrelasCima;
	public GameObject EstrelasBaixo;
	public GameObject EstrelasLadoD;
	public GameObject EstrelasAtras;
	public GameObject Mira;
	public GameObject Alvo;
	public GameObject score;
	public float cdRB;

	public float countTime1;
	public float countTime2;
	public float countTime3;
	public float countTime4;
	public float countTime5;
	public float countTime6;
	public float countTime7;
	public float countTime8;
	public float countTime9;
	public float countTime10;
	public float countTime11;
	public float countTime12;
	public float countTime13;
	public float countTime14;
	public float countTime15;
	public float countTime16;
	public float countTime17;
	public float countTime18;
	public float countTime19;
	public float countTime20;
	public float countTime21;
	public float countTime22;
	public float countTime23;
	public float countTime24;
	public float countTime25;
	public float countTime26;
	public float countTime27;
	public float countTime28;
	public float countTime29;
	public float countTime30;
	public float countTime31;
	public float countTime32;
	public float countTime33;
	public float countTime34;
	public float countTime35;

	public Text count1;
	public Text count2;
	public Text count3;
	public Text count4;
	public Text count5;
	public Text count6;
	public Text count7;
	public Text count8;
	public Text count9;
	public Text count10;
	public Text count11;
	public Text count12;
	public Text count13;
	public Text count14;
	public Text count15;
	public Text count16;
	public Text count17;
	public Text count18;
	public Text count19;
	public Text count20;
	public Text count21;
	public Text count22;
	public Text count23;
	public Text count24;
	public Text count25;
	public Text count26;
	public Text count27;
	public Text count28;
	public Text count29;
	public Text count30;
	public Text count31;
	public Text count32;
	public Text count33;
	public Text count34;
	public Text count35;




	void Start () {

		cdRB = 0;
		Timer = GetComponent<Text> ();
        Session.Init();

	}


	void Update () {
		if (Input.GetKeyDown ("space") && myCoolTimer <= 0 ) {
            Session.Event("ESPACO_FINAL");
			score.SetActive (true);
		}

		if (Session.Chance() == 0)
			startFrente ();
		if (Session.Chance() == 1)
			startLadoD ();
		if (Session.Chance() == 2)
			startBaixo ();
		if (Session.Chance() == 3)
			startAtras ();

		cdRB += 1f;

		count1.text = countTime1.ToString ();
		count2.text = countTime2.ToString ();
		count3.text = countTime3.ToString ();
		count4.text = countTime4.ToString ();
		count5.text = countTime5.ToString ();
		count6.text = countTime6.ToString ();
		count7.text = countTime7.ToString ();
		count8.text = countTime8.ToString ();
		count9.text = countTime9.ToString ();
		count10.text = countTime10.ToString ();
		count11.text = countTime11.ToString ();
		count12.text = countTime12.ToString ();
		count13.text = countTime13.ToString ();
		count14.text = countTime14.ToString ();
		count15.text = countTime15.ToString ();
		count16.text = countTime16.ToString ();
		count17.text = countTime17.ToString ();
		count18.text = countTime18.ToString ();
		count19.text = countTime19.ToString ();
		count20.text = countTime20.ToString ();
		count21.text = countTime21.ToString ();
		count22.text = countTime22.ToString ();
		count23.text = countTime23.ToString ();
		count24.text = countTime24.ToString ();
		count25.text = countTime25.ToString ();
		count26.text = countTime26.ToString ();
		count27.text = countTime27.ToString ();
		count28.text = countTime28.ToString ();
		count29.text = countTime29.ToString ();
		count30.text = countTime30.ToString ();
		count31.text = countTime31.ToString ();
		count32.text = countTime32.ToString ();
		count33.text = countTime33.ToString ();
		count34.text = countTime34.ToString ();
		count35.text = countTime35.ToString ();

        if (Input.GetKeyDown("joystick button 4") || Input.GetKeyDown("p"))
        {
            Session.Event("BOTAO_PRESSIONADO");
        }


        if (Input.GetKeyDown ("joystick button 4") && countTime1 == 0 && cdRB >= 2 && freeTimer == true) {
			countTime1 = myCoolTimer;
			cdRB = 0;
		}
		if (Input.GetKeyDown ("joystick button 4") && countTime1 != 0 && countTime2 == 0 && cdRB >= 2 && freeTimer == true) {
			countTime2 = myCoolTimer;
			cdRB = 0;
		}
		if (Input.GetKeyDown ("joystick button 4") && countTime2 != 0 && countTime3 == 0 && cdRB >= 2 && freeTimer == true) {
			countTime3 = myCoolTimer;
			cdRB = 0;
		}
		if (Input.GetKeyDown ("joystick button 4") && countTime3 != 0 && countTime4 == 0 && cdRB >= 2 && freeTimer == true) {
			countTime4 = myCoolTimer;
			cdRB = 0;
		}
		if (Input.GetKeyDown ("joystick button 4") && countTime4 != 0 && countTime5 == 0 && cdRB >= 2 && freeTimer == true) {
			countTime5 = myCoolTimer;
			cdRB = 0;
		}
		if (Input.GetKeyDown ("joystick button 4") && countTime5 != 0 && countTime6 == 0 && cdRB >= 2 && freeTimer == true) {
			countTime6 = myCoolTimer;
			cdRB = 0;
		}
		if (Input.GetKeyDown ("joystick button 4") && countTime6 != 0 && countTime7 == 0 && cdRB >= 2 && freeTimer == true) {
			countTime7 = myCoolTimer;
			cdRB = 0;
		}
		if (Input.GetKeyDown ("joystick button 4") && countTime7 != 0 && countTime8 == 0 && cdRB >= 2 && freeTimer == true) {
			countTime8 = myCoolTimer;
			cdRB = 0;
		}
		if (Input.GetKeyDown ("joystick button 4") && countTime8 != 0 && countTime9 == 0 && cdRB >= 2 && freeTimer == true) {
			countTime9 = myCoolTimer;
			cdRB = 0;
		}
		if (Input.GetKeyDown ("joystick button 4") && countTime9 != 0 && countTime10 == 0 && cdRB >= 2 && freeTimer == true) {
			countTime10 = myCoolTimer;
			cdRB = 0;
		}
		if (Input.GetKeyDown ("joystick button 4") && countTime10 != 0 && countTime11 == 0 && cdRB >= 2 && freeTimer == true) {
			countTime11 = myCoolTimer;
			cdRB = 0;
		}
		if (Input.GetKeyDown ("joystick button 4") && countTime11 != 0 && countTime12 == 0 && cdRB >= 2 && freeTimer == true) {
			countTime12 = myCoolTimer;
			cdRB = 0;
		}
		if (Input.GetKeyDown ("joystick button 4") && countTime12 != 0 && countTime13 == 0 && cdRB >= 2 && freeTimer == true) {
			countTime13 = myCoolTimer;
			cdRB = 0;
		}
		if (Input.GetKeyDown ("joystick button 4") && countTime13 != 0 && countTime14 == 0 && cdRB >= 2 && freeTimer == true) {
			countTime14 = myCoolTimer;
			cdRB = 0;
		}
		if (Input.GetKeyDown ("joystick button 4") && countTime14 != 0 && countTime15 == 0 && cdRB >= 2 && freeTimer == true) {
			countTime15 = myCoolTimer;
			cdRB = 0;
		}
		if (Input.GetKeyDown ("joystick button 4") && countTime15 != 0 && countTime16 == 0 && cdRB >= 2 && freeTimer == true) {
			countTime16 = myCoolTimer;
			cdRB = 0;
		}
		if (Input.GetKeyDown ("joystick button 4") && countTime16 != 0 && countTime17 == 0 && cdRB >= 2 && freeTimer == true) {
			countTime17 = myCoolTimer;
			cdRB = 0;
		}
		if (Input.GetKeyDown ("joystick button 4") && countTime17 != 0 && countTime18 == 0 && cdRB >= 2 && freeTimer == true) {
			countTime18 = myCoolTimer;
			cdRB = 0;
		}
		if (Input.GetKeyDown ("joystick button 4") && countTime18 != 0 && countTime19 == 0 && cdRB >= 2 && freeTimer == true) {
			countTime19 = myCoolTimer;
			cdRB = 0;
		}
		if (Input.GetKeyDown ("joystick button 4") && countTime19 != 0 && countTime20 == 0 && cdRB >= 2 && freeTimer == true) {
			countTime20 = myCoolTimer;
			cdRB = 0;
		}
		if (Input.GetKeyDown ("joystick button 4") && countTime20 != 0 && countTime21 == 0 && cdRB >= 2 && freeTimer == true) {
			countTime21 = myCoolTimer;
			cdRB = 0;
		}
		if (Input.GetKeyDown ("joystick button 4") && countTime21 != 0 && countTime22 == 0 && cdRB >= 2 && freeTimer == true) {
			countTime22 = myCoolTimer;
			cdRB = 0;
		}
		if (Input.GetKeyDown ("joystick button 4") && countTime22 != 0 && countTime23 == 0 && cdRB >= 2 && freeTimer == true) {
			countTime23 = myCoolTimer;
			cdRB = 0;
		}
		if (Input.GetKeyDown ("joystick button 4") && countTime23 != 0 && countTime24 == 0 && cdRB >= 2 && freeTimer == true) {
			countTime24 = myCoolTimer;
			cdRB = 0;
		}
		if (Input.GetKeyDown ("joystick button 4") && countTime24 != 0 && countTime25 == 0 && cdRB >= 2 && freeTimer == true) {
			countTime25 = myCoolTimer;
			cdRB = 0;
		}
		if (Input.GetKeyDown ("joystick button 4") && countTime25 != 0 && countTime26 == 0 && cdRB >= 2 && freeTimer == true) {
			countTime26 = myCoolTimer;
			cdRB = 0;
		}
		if (Input.GetKeyDown ("joystick button 4") && countTime26 != 0 && countTime27 == 0 && cdRB >= 2 && freeTimer == true) {
			countTime27 = myCoolTimer;
			cdRB = 0;
		}
		if (Input.GetKeyDown ("joystick button 4") && countTime27 != 0 && countTime28 == 0 && cdRB >= 2 && freeTimer == true) {
			countTime28 = myCoolTimer;
			cdRB = 0;
		}
		if (Input.GetKeyDown ("joystick button 4") && countTime28 != 0 && countTime29 == 0 && cdRB >= 2 && freeTimer == true) {
			countTime29 = myCoolTimer;
			cdRB = 0;
		}
		if (Input.GetKeyDown ("joystick button 4") && countTime29 != 0 && countTime30 == 0 && cdRB >= 2 && freeTimer == true) {
			countTime30 = myCoolTimer;
			cdRB = 0;
		}
		if (Input.GetKeyDown ("joystick button 4") && countTime30 != 0 && countTime31 == 0 && cdRB >= 2 && freeTimer == true) {
			countTime31 = myCoolTimer;
			cdRB = 0;
		}
		if (Input.GetKeyDown ("joystick button 4") && countTime31 != 0 && countTime32 == 0 && cdRB >= 2 && freeTimer == true) {
			countTime32 = myCoolTimer;
			cdRB = 0;
		}
		if (Input.GetKeyDown ("joystick button 4") && countTime32 != 0 && countTime33 == 0 && cdRB >= 2 && freeTimer == true) {
			countTime33 = myCoolTimer;
			cdRB = 0;
		}
		if (Input.GetKeyDown ("joystick button 4") && countTime33 != 0 && countTime34 == 0 && cdRB >= 2 && freeTimer == true) {
			countTime34 = myCoolTimer;
			cdRB = 0;
		}
		if (Input.GetKeyDown ("joystick button 4") && countTime34 != 0 && countTime35 == 0 && cdRB >= 2 && freeTimer == true) {
			countTime35 = myCoolTimer;
			cdRB = 0;
		}

	}

	public void startFrente () {
        if (Input.GetKeyDown ("space") && myCoolTimer == 601) {
            freeTimer = true;
		}

		if (freeTimer == true)
			myCoolTimer -= Time.deltaTime;

//		Timer.text = myCoolTimer.ToString ("0\t");
		
		if (myCoolTimer <= 600) {
			
			EstrelasFrente.SetActive(true);
		}
		
		if (myCoolTimer <= 500) {
			
			EstrelasFrente.SetActive(false);
		}
		
		if (myCoolTimer <= 500) {
			
			EstrelasAtras.SetActive(true);
		}
		
		if (myCoolTimer <= 400) {
			
			EstrelasAtras.SetActive(false);
		}
		
		if (myCoolTimer <= 400) {
			
			EstrelasLado.SetActive(true);
		}
		
		if (myCoolTimer <= 300) {
			
			EstrelasLado.SetActive(false);
		}
		
		if (myCoolTimer <= 300) {
			
			EstrelasLadoD.SetActive(true);
		}
		
		if (myCoolTimer <= 200) {
			
			EstrelasLadoD.SetActive(false);
		}
		
		if (myCoolTimer <= 200) {
			
			EstrelasCima.SetActive(true);
		}
		
		if (myCoolTimer <= 100) {
			
			EstrelasCima.SetActive(false);
		}
		
		if (myCoolTimer <= 100) {
			
			EstrelasBaixo.SetActive(true);
		}
		
		if (myCoolTimer <= 0) {
			
			EstrelasBaixo.SetActive(false);
		}
		
		if (myCoolTimer <= 0) {
			
			Mira.SetActive(false);
		}
		
		if (myCoolTimer <= 0) {
			
			Alvo.SetActive(false);
		}

	}


	public void startLadoD () {
        if (Input.GetKeyDown ("space") && myCoolTimer == 601) {

			freeTimer = true;
		}

		if (freeTimer == true)
			myCoolTimer -= Time.deltaTime;

		//		Timer.text = myCoolTimer.ToString ("0\t");
		
		if (myCoolTimer <= 600) {
			
			EstrelasLadoD.SetActive(true);
		}
		
		if (myCoolTimer <= 500) {
			
			EstrelasLadoD.SetActive(false);
		}
		
		if (myCoolTimer <= 500) {
			
			EstrelasBaixo.SetActive(true);
		}
		
		if (myCoolTimer <= 400) {
			
			EstrelasBaixo.SetActive(false);
		}
		
		if (myCoolTimer <= 400) {
			
			EstrelasFrente.SetActive(true);
		}
		
		if (myCoolTimer <= 300) {
			
			EstrelasFrente.SetActive(false);
		}
		
		if (myCoolTimer <= 300) {
			
			EstrelasLado.SetActive(true);
		}
		
		if (myCoolTimer <= 200) {
			
			EstrelasLado.SetActive(false);
		}
		
		if (myCoolTimer <= 200) {
			
			EstrelasAtras.SetActive(true);
		}
		
		if (myCoolTimer <= 100) {
			
			EstrelasAtras.SetActive(false);
		}
		
		if (myCoolTimer <= 100) {
			
			EstrelasCima.SetActive(true);
		}
		
		if (myCoolTimer <= 0) {
			
			EstrelasCima.SetActive(false);
		}
		
		if (myCoolTimer <= 0) {
			
			Mira.SetActive(false);
		}
		
		if (myCoolTimer <= 0) {
			
			Alvo.SetActive(false);
		}
		
	}


	public void startBaixo () {
		
		if (Input.GetKeyDown ("space") && myCoolTimer == 601) {

			freeTimer = true;
		}

		if (freeTimer == true)
			myCoolTimer -= Time.deltaTime;

		//		Timer.text = myCoolTimer.ToString ("0\t");
		
		if (myCoolTimer <= 600) {
			
			EstrelasBaixo.SetActive(true);
		}
		
		if (myCoolTimer <= 500) {
			
			EstrelasBaixo.SetActive(false);
		}
		
		if (myCoolTimer <= 500) {
			
			EstrelasFrente.SetActive(true);
		}
		
		if (myCoolTimer <= 400) {
			
			EstrelasFrente.SetActive(false);
		}
		
		if (myCoolTimer <= 400) {
			
			EstrelasLadoD.SetActive(true);
		}
		
		if (myCoolTimer <= 300) {
			
			EstrelasLadoD.SetActive(false);
		}
		
		if (myCoolTimer <= 300) {
			
			EstrelasCima.SetActive(true);
		}
		
		if (myCoolTimer <= 200) {
			
			EstrelasCima.SetActive(false);
		}
		
		if (myCoolTimer <= 200) {
			
			EstrelasLado.SetActive(true);
		}
		
		if (myCoolTimer <= 100) {
			
			EstrelasLado.SetActive(false);
		}
		
		if (myCoolTimer <= 100) {
			
			EstrelasAtras.SetActive(true);
		}
		
		if (myCoolTimer <= 0) {
			
			EstrelasAtras.SetActive(false);
		}
		
		if (myCoolTimer <= 0) {
			
			Mira.SetActive(false);
		}
		
		if (myCoolTimer <= 0) {
			
			Alvo.SetActive(false);
		}
		
	}


	public void startAtras () {
		
		if (Input.GetKeyDown ("space") && myCoolTimer == 601) {

			freeTimer = true;
		}

		if (freeTimer == true)
			myCoolTimer -= Time.deltaTime;

		//		Timer.text = myCoolTimer.ToString ("0\t");
		
		if (myCoolTimer <= 600) {
			
			EstrelasAtras.SetActive(true);
		}
		
		if (myCoolTimer <= 500) {
			
			EstrelasAtras.SetActive(false);
		}
		
		if (myCoolTimer <= 500) {
			
			EstrelasLadoD.SetActive(true);
		}
		
		if (myCoolTimer <= 400) {
			
			EstrelasLadoD.SetActive(false);
		}
		
		if (myCoolTimer <= 400) {
			
			EstrelasBaixo.SetActive(true);
		}
		
		if (myCoolTimer <= 300) {
			
			EstrelasBaixo.SetActive(false);
		}
		
		if (myCoolTimer <= 300) {
			
			EstrelasLado.SetActive(true);
		}
		
		if (myCoolTimer <= 200) {
			
			EstrelasLado.SetActive(false);
		}
		
		if (myCoolTimer <= 200) {
			
			EstrelasCima.SetActive(true);
		}
		
		if (myCoolTimer <= 100) {
			
			EstrelasCima.SetActive(false);
		}
		
		if (myCoolTimer <= 100) {
			
			EstrelasFrente.SetActive(true);
		}
		
		if (myCoolTimer <= 0) {
			
			EstrelasFrente.SetActive(false);
		}
		
		if (myCoolTimer <= 0) {
			
			Mira.SetActive(false);
		}
		
		if (myCoolTimer <= 0) {
			
			Alvo.SetActive(false);
		}
		
	}

}
