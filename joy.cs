﻿using UnityEngine;
using System.Collections;

public class joy : MonoBehaviour {


	public float speed = 30;
    private static readonly float FIXED_Z_AXIS = -3.8f;


    void Update () {
		Movement ();
	}

	void Movement() {
		transform.Translate(0,0,Input.GetAxis("RightJoystickVertical") * Time.deltaTime * speed);
		transform.Translate(Input.GetAxis("RightJoystickHorizontal") * Time.deltaTime * speed,0,0);
        transform.Translate((transform.position.z - FIXED_Z_AXIS) * transform.forward);
    }
}