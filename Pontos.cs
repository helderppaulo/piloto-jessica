﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Assets._Scripts;

public class Pontos : MonoBehaviour {
	
	public int pontos;
	public bool centro = false;
	public bool meio = false;
	public bool fora = false;
	public bool target4 = false;
	public bool target5 = false;
	public float cdPontos = 0;
	public Text score;
	public bool tutorial = true;

	void Start () {
		pontos = 0;
        Session.Init();
	}

	void Update () {

		if (Input.GetKeyDown ("space") && pontos == 0) {
            Session.SoftStart();
			tutorial = false;
		}

		score.text = "Pontos: " + pontos.ToString ();

        int scoreDelta = 0;

        cdPontos = 100;

		if (tutorial == false) {

			if (centro && cdPontos >= 1) {
				pontos += 5;
                scoreDelta = 5;
				cdPontos = 0;

			}
		
			if (meio && cdPontos >= 1) {
				pontos += 4;
                scoreDelta = 4;
				cdPontos = 0;
			}
		
			if (fora && cdPontos >= 1) {
				pontos += 3;
                scoreDelta = 3;
				cdPontos = 0;
			}

			if (target4 && cdPontos >= 1) {
				pontos += 2;
                scoreDelta = 2;
				cdPontos = 0;
			}

			if (target5 && cdPontos >= 1) {
				pontos += 1;
                scoreDelta = 1;
				cdPontos = 0;
			}
		}

        Session.Append(pontos.ToString(), scoreDelta.ToString());

	}

	void OnTriggerEnter(Collider hit){

		if (hit.gameObject.tag == "TargetCentro") {
			centro = true;
		}

		if (hit.gameObject.tag == "TargetMeio") {
			meio = true;
		}

		if (hit.gameObject.tag == "TargetFora") {
			fora = true;
		}

		if (hit.gameObject.tag == "Target4") {
			target4 = true;
		}

		if (hit.gameObject.tag == "Target5") {
			target5 = true;
		}
	}

	void OnTriggerExit(Collider hit){

		if (hit.gameObject.tag == "TargetCentro") {
			centro = false;
		}
		
		if (hit.gameObject.tag == "TargetMeio") {
			meio = false;
		}
		
		if (hit.gameObject.tag == "TargetFora") {
			fora = false;
		}

		if (hit.gameObject.tag == "Target4") {
			target4 = false;
		}
		
		if (hit.gameObject.tag == "Target5") {
			target5 = false;
		}
	}
}
