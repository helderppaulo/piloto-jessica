﻿using UnityEngine;
using System.Collections;
using Assets._Scripts;

public class EstrelaLado : MonoBehaviour {
	
	public float CDciclo = 100;
	public float velocidade;
	private Rigidbody rb;
	public GameObject estrela;
	public GameObject respawn;

	
	
	
	void Start () {
        Session.Event("INICIO_ESQUERDA");
        velocidade = -100;
		rb = GetComponent<Rigidbody> ();
		respawn =  GameObject.FindGameObjectWithTag("SpawnLado");

	}
	
	
	void Update () {

		print (velocidade);
		CDciclo -= Time.deltaTime;
		rb.velocity = new Vector3 (velocidade*Time.deltaTime, 0, 0);

		if (CDciclo <= 100 && CDciclo > 70) {
			velocidade -= 8.5f *Time.deltaTime;
		}
		
		if (CDciclo <= 70 && CDciclo > 40) {
			velocidade = velocidade;
		}
		
		if (CDciclo <= 40) {
			velocidade += 11.5f*Time.deltaTime;
		}

		if (CDciclo <= 10) {
			velocidade = 0;
		}

		if (velocidade >= 0) {
			velocidade = 0;
		}
		
	}
	void OnTriggerEnter(Collider hit){

		
		if (hit.gameObject.tag == "RespawnLado") {
			transform.position = respawn.transform.position;
			
		}	
	}
}
