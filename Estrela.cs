﻿using UnityEngine;
using System.Collections;
using Assets._Scripts;

public class Estrela : MonoBehaviour {


	public float velocidade;
	private Rigidbody rb;
	public GameObject estrela;
	public GameObject respawn;
	public float CDciclo = 100;




	void Start () {
        Session.Event("INICIO_FRENTE");
        velocidade = 100;
		rb = GetComponent<Rigidbody> ();
		respawn =  GameObject.FindGameObjectWithTag("Spawn");

	}
	

	void Update () {


		CDciclo -= Time.deltaTime;
		rb.velocity = new Vector3 (0, 0, velocidade*Time.deltaTime);

		if (CDciclo <= 100 && CDciclo > 70) {
			velocidade += 8.5f *Time.deltaTime;
		}

		if (CDciclo <= 70 && CDciclo > 40) {
			velocidade = velocidade;
		}

		if (CDciclo <= 40) {
			velocidade -= 11.5f *Time.deltaTime;
		}

		if (CDciclo <= 10) {
			velocidade = 0;
		}

		if (velocidade <= 0) {
			velocidade = 0;
		}

		print (velocidade);
	}
	void OnTriggerEnter(Collider hit){
	


		if (hit.gameObject.tag == "Respawn") {
			transform.position = respawn.transform.position;


		}	
	}
}
